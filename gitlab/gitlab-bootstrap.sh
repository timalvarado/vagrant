#!/bin/bash

sudo yum install -y curl policycoreutils-python openssh-server firewalld
sudo systemctl enable sshd
sudo systemctl start sshd
sudo systemctl enable firewalld
sudo systemctl start firewalld
sudo firewall-cmd --permanent --add-service=http
sudo firewall-cmd --permanent --add-service=https
sudo systemctl reload firewalld
curl -s https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.rpm.sh | sudo bash
sudo yum install -y gitlab-ce
sudo gitlab-ctl reconfigure
